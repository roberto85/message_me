# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(name: "Rob", password: "password")
User.create(name: "Jan", password: "password")
User.create(name: "Bart", password: "password")
User.create(name: "Hannes", password: "password")
Message.create(body:"hi this is me, Rob", user_id:"1")
Message.create(body:"hi this is me, Jan", user_id:"2")
Message.create(body:"hi this is me, Bart", user_id:"3")
Message.create(body:"hi this is me, Hannes", user_id:"4")